/* _________________________________________________________________________________________________________________

====================================================================================================================
                                                EXPLICACIONES
====================================================================================================================

1. Empezar
    El botón para que el juego empiece, dotando al #start la funcionalidad de poder cickarse y llevar a cabo
    todos los pasos para que el juego funcione.
    1.1 Randomizador
        Creamos una variable para que genere un número de forma aleatoria entre el 1 y el 4.
    1.2. Concatenación
        Creamos una variable que conectará con la #serie del html para poder ir guardando la lista de números
        en su value. Además, para poder hacer eso mismo, creamos una concatenación para dicho propósito.
    1.3. Bucle
        Para crear el bucle con el que poder hacer la serie con la que jugar an simon, primero creamos una
        variable que especifique la longitud de valores que hay en #serie.
        Luego, creamos el for usando la misma variable para que recorra el contenido de #serie, con la "i = 0"
        siendo la posición inicial y el "i++" añadiendo un valor más a la posición inicial.
        1.3.1 Detector
            Va recorriendo la serie de uno en uno para saber qué valor es cada uno.

__________________________________________________________________________________________________________________ */

// Empezar
$(document).on('click', '#start', function(){
    // Randomizador
    var randomcolor = Math.floor((Math.random() * 4) + 1);
    // Concatenación
    var serie = $("#serie").val();
    serie = $("#serie").val(serie += randomcolor);
    // Bucle
    var serie_bucle = $("#serie").val().length;

    for(var i = 0; i < serie_bucle; i++){
        //alert($("#serie").val());
        // Detector
        var detector = $("#serie").val().charAt(i);
        alert(detector);
        
        //Cambia el color añadiéndole la clase con el nuevo color
        //Remueve el color para el siguiente color resaltado
        /*
        Cómo progresivamente retrasar el loop:
        https://stackoverflow.com/questions/3583724/how-do-i-add-a-delay-in-a-javascript-loop

        Cómo quitar clases:
        http://api.jquery.com/removeClass/
        */
        /*if(detector == 1){
            $(".a").addClass("a a_activo");

        }else if(detector == 2){

        }else if(detector == 3){

        }else if(detector == 4){

        }*/
    }
});

// Botones de los 4 colores
$(document).on('click', '.button', function(){
    // Variable para definir el valor del color que clickeamos
    var selection = $(this).attr('id');
     
    // Variables que definen el value en el input del html
    var contador = $("#contador").val(); 
    var serie = $("#serie").val();

    // Si el contador (que empieza en 0) es menor al total de contenido de la serie y
    // ...además, el valor de la serie en la posición igual al contador actual es igual
    // ...al botón que hemos pulsado, podemos pasar. (es el "sí y sigue").
    if(contador + 1 < serie.length && serie.charAt(contador) == selection){

        alert("Correcto");
        // Le metemos +1 al contador para que funcione el if (por si sigue siendo menor que if
        // ...y poder ajustar el siguiente número en la lista de serie, el charAt).
        $("#contador").val(contador + 1);

    // Debemos hacer el "sí y acaba"
    }else if(contador + 1 == serie.length && serie.charAt(contador) == selection){

        alert("Correcto, ahora me toca a mi");
        $("#contador").val(contador = 0);
        $('#start').trigger("click");

    }/*else{
        // Sacado de una página para intentar llamar la otra función
        console.log("WRONG");
            $("#fail").show();
            $("#fail").addClass("bigEntrance");
            settings.clicked = 0;
            $("#a, #b, #c, #d").off("mousedown");
    }*/
    
    
});
